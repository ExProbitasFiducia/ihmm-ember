import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | fellows-nominating-committee', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:fellows-nominating-committee');
    assert.ok(route);
  });
});
