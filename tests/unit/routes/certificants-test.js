import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | certificants', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:certificants');
    assert.ok(route);
  });
});
