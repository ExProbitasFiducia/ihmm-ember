import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | certifications/exams', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:certifications/exams');
    assert.ok(route);
  });
});
