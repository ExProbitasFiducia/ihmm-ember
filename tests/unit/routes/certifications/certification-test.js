import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | certifications/certification', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:certifications/certification');
    assert.ok(route);
  });
});
