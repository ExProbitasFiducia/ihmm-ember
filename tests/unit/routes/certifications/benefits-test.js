import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | certifications/benefits', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:certifications/benefits');
    assert.ok(route);
  });
});
