import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | committees/committee', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:committees/committee');
    assert.ok(route);
  });
});
