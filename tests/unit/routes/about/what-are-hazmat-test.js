import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | about/what-are-hazmat', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:about/what-are-hazmat');
    assert.ok(route);
  });
});
