import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | committees', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:committees');
    assert.ok(route);
  });
});
