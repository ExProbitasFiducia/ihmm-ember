import fs from "fs";
import { spawn } from "child_process";

const emberBuild = () => {

    return new Promise((resolve, reject) => {

        const buildProcess = spawn("./node_modules/.bin/ember", ["b"]);
        var buildLogs = "";

        buildProcess.stdout.on("data", data => {
            console.log(`${data}`);
        });

        buildProcess.stderr.on("data", data => {
            console.log(`${data}`);
        });

        buildProcess.on("close", code => {
            console.log("Ember has finished building the application")
            /*fs.readdir("/", function(err, items) {
                console.log(items);

                for (var i=0; i<items.length; i++) {
                    console.log(items[i]);
                }
            });*/
            resolve({buildLogs});
        });

    });

}

const spawnMain = () => {

    return new Promise((resolve, reject) => {

        const mainProcess = spawn("node", ["--experimental-modules", "main.mjs"])
        console.log("The main process has been spawned")

        mainProcess.stdout.on("data", data => {
            console.log(`${data}`);
        });

        mainProcess.stderr.on("data", data => {
            console.log(`${data}`);
        })

        mainProcess.on("close", code => {
            console.log(`The main process exited with the code ${code}`)
        });

    });

}

(async () => {

    await emberBuild();
    await spawnMain();

})();