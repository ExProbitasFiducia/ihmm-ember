import FastBootAppServer from "fastboot-app-server";
import FSNotifier from "fastboot-fs-notifier";


const notifier = new FSNotifier({
  targetDir: "/code/dist"
});

const server = new FastBootAppServer({
    distPath: 'dist',
    gzip: true, // Optional - Enables gzip compression.
    host: '0.0.0.0', // Optional - Sets the host the server listens on
    port: 4000, // Optional - Sets the port the server listens on (defaults to the PORT env var or 3000).
    sandboxGlobals: {}, // Optional - Make values available to the Ember app running in the FastBoot server, e.g. "MY_GLOBAL" will be available as "GLOBAL_VALUE"
    chunkedResponse: true, // Optional - Opt-in to chunked transfer encoding, transferring the head, body and potential shoeboxes in separate chunks. Chunked transfer encoding should have a positive effect in particular when the app transfers a lot of data in the shoebox.
    notifier
});

server.start();
console.log("server started");