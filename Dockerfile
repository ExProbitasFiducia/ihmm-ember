FROM node:alpine

RUN mkdir /code
WORKDIR /code
RUN yarn global add ember-cli
COPY package.json package.json
RUN yarn
COPY . .

EXPOSE 4000

CMD [ "yarn", "entrypoint" ]