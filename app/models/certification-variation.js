import DS from 'ember-data';

import { computed } from "@ember/object";


export default DS.Model.extend({
    status: DS.attr(),
    sku: DS.attr(),
    title: DS.attr(),
    price: DS.attr(),
    created: DS.attr(),
    changed: DS.attr(),
    commerce_product_variation_type: DS.belongsTo("product-variation-type"),
    uid: DS.belongsTo("user"),
});
