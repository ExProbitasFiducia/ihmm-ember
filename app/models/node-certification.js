import DS from 'ember-data';

import { computed } from "@ember/object";


export default DS.Model.extend({
    description: DS.attr(),
    title: DS.attr(),
    body: DS.attr(),
    field_prerequisites: DS.attr(),
    field_application: DS.attr(),
    field_application_fee: DS.attr(),
    field_exam: DS.attr(),
    field_examination_fee: DS.attr(),
    field_open_book: DS.attr(),
    field_open_notes: DS.attr(),
    field_education_requirements: DS.attr(),
    field_work_requirements: DS.attr(),
    field_recertification: DS.attr()
});