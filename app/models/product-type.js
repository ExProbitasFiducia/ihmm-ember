import DS from 'ember-data';

import { computed } from "@ember/object";


export default DS.Model.extend({
    status: DS.attr('boolean'),
    label: DS.attr(),
    description: DS.attr(),
    variationType: DS.attr(),
    multipleVariations: DS.attr("boolean"),
    locked: DS.attr("boolean")
    
    //field_tags: DS.hasMany('tag', { async: true }),
    //field_image: DS.belongsTo('file', { async: true })
});
