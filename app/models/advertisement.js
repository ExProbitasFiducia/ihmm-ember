import DS from 'ember-data';

import { computed } from "@ember/object";


export default DS.Model.extend({
    status: DS.attr('boolean'),
    created: DS.attr(),
    createdDate: computed('created', function() {
        const created = this.get('created');
        if (created) {
            let date = new Date(created * 1000);
            return date.toString();
        } else {
            return "";
        }
    }),
    changed: DS.attr(),
    changedDate: computed('changed', function() {
        const changed = this.get('changed');
        if (changed) {
            let date = new Date(changed * 1000);
            return date.toString();
        } else {
            return "";
        }
    }),
    //uid: DS.belongsTo('user', { async: true }),
    title: DS.attr(),
    body: DS.attr(),
    variations: DS.hasMany('advertisement-variation', {async: false}),
    commerce_product_type: DS.belongsTo("product-type", {async: false}), //field_tags: DS.hasMany('tag', { async: true }),
    //field_image: DS.belongsTo('file', { async: true })
});