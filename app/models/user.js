import DS from 'ember-data';

import { computed } from "@ember/object";


export default DS.Model.extend({
    status: DS.attr('boolean'),
    created: DS.attr(),
    createdDate: computed('created', function() {
        const created = this.get('created');
        if (created) {
            let date = new Date(created * 1000);
            return date.toString();
        } else {
            return "";
        }
    }),
    changed: DS.attr(),
    changedDate: computed('changed', function() {
        const changed = this.get('changed');
        if (changed) {
            let date = new Date(changed * 1000);
            return date.toString();
        } else {
            return "";
        }
    }),
    name: DS.attr(),
    mail: DS.attr(),
    timezone: DS.attr(),
    access: DS.attr(),
    login: DS.attr(),
    init: DS.attr(),
    default_langcode: DS.attr("boolean"),
    field_display_name: DS.attr(),
    roles: DS.hasMany("user-role"),
    

});
