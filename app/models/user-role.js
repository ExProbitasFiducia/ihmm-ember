import DS from 'ember-data';

import { computed } from "@ember/object";


export default DS.Model.extend({
    langcode: DS.attr(),
    status: DS.attr('boolean'),
    dependencies: DS.attr(),
    drupal_internal_id: DS.attr(),
    label: DS.attr(),
    weight: DS.attr(),
    is_admin: DS.attr("boolean"),
    permissions: DS.attr()
});
