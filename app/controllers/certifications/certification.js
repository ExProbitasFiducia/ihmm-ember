import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
    showExamSection: computed(
        "certification.{field_examination.value,field_open_book,field_open_notes}",
        function() {
            return [
                this.get("certification.field_exam.value"),
                this.get("certification.field_open_book"),
                this.get("certification.field_open_notes")
            ].any(Boolean);
        }
    )
});
