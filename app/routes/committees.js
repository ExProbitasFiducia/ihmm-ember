import Route from '@ember/routing/route';

export default Route.extend({
    model() {
        return this.store.findAll("committee");
    },

    setupController(controller, model) {
        console.log(model);
        controller.set("committees", model);
    }
});
