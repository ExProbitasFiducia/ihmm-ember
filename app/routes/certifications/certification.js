import Route from '@ember/routing/route';

export default Route.extend({
    model(params) {
        return this.store.findRecord("certification", params.cert_name);
    },
    setupController(controller, model) {
        controller.set("certification", model);
    }
});
