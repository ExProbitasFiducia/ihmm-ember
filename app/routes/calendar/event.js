import Route from '@ember/routing/route';
import { events } from '../../data';

export default Route.extend({
    model(params) {
        return events.find(ev => ev.id === params.event_name);
    }
});
