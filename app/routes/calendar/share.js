import Route from '@ember/routing/route';

export default Route.extend({

    model(params) {
        return this.store.createRecord("event", {
            title: "",
            field_date: new Date()
        });
    },

    setupController(controller, model) {
        controller.set("event", model);
    }

});
