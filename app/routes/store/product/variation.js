import Route from '@ember/routing/route';

export default Route.extend({
    model(params) {
        const product = this.modelFor("store.product");
        const productName = "advertisement";
        return this.store.query(`${productName}-variation`, {
            filter: {
                sku: params.sku
            }
        });
    },
    setupController(controller, model) {
        controller.set("variations", model);
    }
});
