import Route from '@ember/routing/route';

export default Route.extend({
    model(params) {
        return this.store.query("product-type", {
        });
    },
    setupController(controller, model) {
        controller.set("product", model);
    }
});
