import Route from '@ember/routing/route';

export default Route.extend({
    model() {
        return this.store.findRecord("page", "ce9b0155-b08d-48be-b3b6-31b7ce66e3e0");
    },

    setupController(controller, model) {
        controller.set("bylaws", model);
    }
});
