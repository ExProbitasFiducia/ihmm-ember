import Route from '@ember/routing/route';
import { events } from "../data";

export default Route.extend({
    model() {
        return events;
    }
});
