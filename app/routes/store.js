import Route from '@ember/routing/route';

import RSVP from "rsvp";
export default Route.extend({
    model() {
        return this.store.findAll("product-type").then(productTypes => {
            return RSVP.hash(
                productTypes.reduce((productsByType, productType) => {
                    return {
                        ...productsByType,
                        [productType.label]: this.store.query(`${productType.label}-variation`, {
                            include: "commerce_product_variation_type, uid",
                        })

                    }
                }, {})
            );
        });
    },
    setupController(controller, model) {
        controller.set("productsByType", model);
    }
});
