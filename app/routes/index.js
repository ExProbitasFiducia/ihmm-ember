import Route from '@ember/routing/route';
import RSVP from "rsvp";

import {
    events
} from "../data";

export default Route.extend({

    model() {
        return RSVP.hash({
            events,
            articles: this.store.query("article", {
            
            }),
        });
    },

    setupController(controller, model, transition) {
        controller.set("events", model.events);
        controller.set("articles", model.articles);
    }

});
