import Route from '@ember/routing/route';

export default Route.extend({
    model(params) {
        return this.modelFor("committees.committee");
    },

    setupController(controller, model) {
        controller.set("committee", model);
    }
});
