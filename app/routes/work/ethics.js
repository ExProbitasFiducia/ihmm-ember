import Route from '@ember/routing/route';

export default Route.extend({
    model(params) {
        return this.store.query("page", params.committee_id);
    },

    setupController(controller, model) {
        console.log(model);
        controller.set("page", model);
    }
});
