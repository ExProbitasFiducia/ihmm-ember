import Route from "@ember/routing/route";

import RSVP from "rsvp";


export default Route.extend({
    activate() {
        //this.controllerFor("work").set("theme", "media-kit");
    },
    model() {
        return RSVP.hash({
            advertisements: this.store.query("advertisement", {
                include: "variations, commerce_product_type"
            }),
            pages: this.store.query("page", {
                filter: {
                    title: "mediakit"
                }
            })
        })
    },
    setupController(controller, model) {
        controller.set("advertisements", model.advertisements);
        controller.set("pages", model.pages);
    },
    deactivate() {
        //this.controllerFor("work").set("theme", "");
    }
});