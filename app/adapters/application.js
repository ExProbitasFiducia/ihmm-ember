import Ember from 'ember';
import DS from 'ember-data';
import ENV from '../config/environment';

export default DS.JSONAPIAdapter.extend({
    session: Ember.inject.service(),

    host: ENV.apiBaseUrl,
    headers: {
        Accept: 'application/json'
    },

    ajax(url, method, hash) {
        hash = hash || {};
        //hash.crossDomain = true;
        //hash.xhrFields = { withCredentials: true };
        hash.headers = hash.headers || {};
        console.log("AJAX!");
        console.log(url);
        if (url.charAt(4) !== "s") {
            url = `https${url.slice(4)}`
        }
        console.log(url);
        //hash.headers["X-CSRFTOKEN"] = this.get("session.data.authenticated.csrfToken") || "";
        return this._super(url, method, hash);
    },

    buildURL() {
        const base = this._super(...arguments);
        return `${base}`;
    },

    pathForType(type) {
        console.log(type);
        let entityPath;
        let entity = {
            article: "node",
            event: "node",
            certification: "node",
            page: "node",
            committee: "node",
            book: "node",
            advertisement: "commerce_product",
            user_role: "user_role"
        }[type];
        //let [entity, group] = type.split("--");
        entityPath = `${entity}/${type}`;
        return entityPath;
    }

});