import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
    pathForType() {
        return "commerce_product_variation_type/commerce_product_variation_type";
    }
});

