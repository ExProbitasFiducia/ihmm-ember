export const stories = [{
    title: `Highlights from the Trump Administration’s Rulemaking Agenda`,
    slug: "trump-admin-rule-highlights",
    content: `<p>The Office of Management and Budget&rsquo;s&nbsp;<a href="https://www.reginfo.gov/public/do/eAgendaMain" rel="noopener noreferrer" target="_blank">Spring 2019 Unified Regulatory Agenda</a>&nbsp;includes many items with significant implications for the energy and infrastructure sectors. This blog post offers highlights from the Agenda, with an emphasis on upcoming actions by the Department of the Interior, Environmental Protection Agency, Army Corps of Engineers, and the Council on Environmental Quality.</p><p>SEE:<a href="https://www.natlawreview.com/article/highlights-trump-administration-s-rulemaking-agenda">&nbsp;https://www.natlawreview.com/article/highlights-trump-administration-s-rulemaking-agenda</a></p>`,
}, {
    title: "Weekly IRS Roundup July 1 – 5, 2019",
    slug: "weekly-irs-roundup",
    content: `Presented below is our summary of significant Internal Revenue Service (IRS) guidance and relevant tax matters for the week of July 1 – 5, 2019. SEE: https://www.natlawreview.com/article/weekly-irs-roundup-july-1-5-2019`
}, {
    title: "Uncertainty Abounds Despite Clean Water Act Update",
    slug: "clean-water-update-uncertainty",
    content: `Last month marked the 50-year anniversary of one of the more infamous and impactful environmental disasters to occur in the United States. On June 22, 1969, the Cuyahoga River, which runs through the heart of Cleveland before emptying into Lake Erie, caught fire for the 13th time. Time magazine ran a story that highlighted the river’s severe pollution.1 The national reaction to the story is widely credited as the impetus for the Federal Water Pollution Control Act Amendments of 1972, now known as the Clean Water Act (CWA).2 The objective of the CWA is “to restore and maintain the chemical, physical, and biological integrity of the Nation’s waters.”3 This seemingly straightforward and worthy objective has, however, led to more than 40 years of uncertainty and litigation over what constitutes “the nation’s waters” or “waters of the United States.”SEE: https://www.natlawreview.com/article/uncertainty-abounds-despite-clean-water-act-update `
}];

export const events = [{
    id: "1",
    name: "Advanced Hazardous Materials Management - CHMM Prep Course",
    date: "Tuesday, July 9, 2019 - 08:30 to Thursday, July 11, 2019 - 04:30",
    producer: "Environmental Management Institute",
    location: "Indianapolis, IN"
}, {
    id: "2",
    name: "EHMM Review Course",
    date: "Tuesday, July 23, 2019 - 12:00 to Friday, July 26, 2019 - 12:00",
    producer: "For additional information, contact julie@fetinc.org",
    location: "Wisconsin CHMM Chapter"
}, {
    id: "3",
    name: "Undeclared Hazmat Shipments Webinar Presented by DOT",
    date: "Tuesday, July 16, 2019 - 13:00 to 15:00",
    producer: "US DOT",
    location: "webinar"
}, {
    id: "4",
    name: "3.5 Day EHMM Review Course",
    date: "Tue, 06/25/2019",
    producer: "Wisconsin CHMM Chapter",
    location: "Milwaukee, WI "
}, {
    id: "5",
    name: "EHMM Review Course",
    date: "Tuesday, August 6, 2019 - 08:00 to Thursday, August 8, 2019 - 05:00",
    producer: "Nashville AHMP Chapter",
    location: "Nashville, TN",
}, {
    id: "6",
    name: "CHMM Online Review",
    date: "Tuesday, August 13, 2019 - 08:00 to Thursday, October 3, 2019 - 10:00",
    producer: "Bowen EHS",
    location: "Online"
}];