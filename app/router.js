import EmberRouter from '@ember/routing/router';
import RouterScroll from "ember-router-scroll";
import config from './config/environment';

const Router = EmberRouter.extend(RouterScroll, {
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('book', {
    path: ":/book_slug"
  });
  this.route('pages', function() {
    this.route('page', function() {
      this.route('edit');
    });
  });
  this.route('applicants');
  this.route('certificants');
  this.route('work', function() {
    this.route('credentials');
    this.route('accreditation');
    this.route('ethics');
    this.route('designations');
    this.route('leaders');
    this.route('what-are-hazmat');
    this.route('recognition');
    this.route('veterans');
    this.route('legal');
    this.route('media');
    this.route('committees');
  });
  this.route('certifications', function() {
    this.route('certification', {
      path: "/:cert_name"
    });
    this.route('benefits');
    this.route('exams');
    this.route('exam-prep');
  });
  this.route('calendar', function() {
    this.route('share');
    this.route('event', {
      path: "/:event_name"
    });
  });
  this.route('news', function() {
    this.route('story', {
      path: "/:story_slug"
    });
  });
  this.route('committees', function() {
    this.route('committee', {
      path: "/:committee_id"
    }, function() {
      this.route('apply');
    });
  });
  this.route('bylaws');

  this.route('media', function() {
    this.route('ads', function() {
      this.route('reserve');
    });
  });
  this.route('store', function() {
    this.route('product', {
      path: "/:label"
    }, function() {
      this.route('variation', {
        path: "/:sku"
      });
    });
  });
});

export default Router;
