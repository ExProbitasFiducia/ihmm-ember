import DS from "ember-data";

export default DS.JSONAPISerializer.extend({

    keyForRelationship(key) {
        return key;
    },

    keyForAttribute(key) {
        return key;
    },

    modelNameFromPayloadKey(key) {
        console.log(key);
        let [entity, group] = key.split("--");
        if (entity === "commerce_product_type") {
            console.log("commerce_product_type");
            return "product-type";
        }
        if (entity === "commerce_product_variation")
            return `${group}-variation`;
        return group;
    }

});